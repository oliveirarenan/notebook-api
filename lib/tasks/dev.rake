namespace :dev do
  desc "Configure the initial development environment"
  task setup: :environment do

    %x(rails db:drop db:create db:migrate)

    puts 'Creating Kinds'

    kinds = %w(Amigos Comercial Conhecido)

    kinds.each do |kind|

      Kind.create!(
          description: kind
      )

    end

    puts 'Kinds created!'

    puts '================='

    puts 'Creating contacts'

    100.times do

      Contact.create!(
          name: Faker::Name.name,
          email: Faker::Internet.email,
          birthdate: Faker::Date.between(20.years.ago, 18.years.ago),
          kind: Kind.all.sample
      )

    end

    puts 'Contacts created!'

    puts '================='

    puts 'Creating phone numbers'

    Contact.all.each do |contact|
      Random.rand(5).times do |i|
        phone = Phone.create!(number: Faker::PhoneNumber.cell_phone)
        contact.phones << phone
        contact.save!
      end
    end

    puts 'Phone number created!'

    puts '================='

    puts 'Creating Addresses'

    Contact.all.each do |contact|
      address = Address.create!(street: Faker::Address.street_name, city: Faker::Address.city)
      contact.address = address
      contact.save!

    end

    puts 'Addresses created!'

  end

end
