class Contact < ApplicationRecord

  belongs_to :kind, optional: true
  has_one :address
  has_many :phones
  accepts_nested_attributes_for :phones, allow_destroy: true
  accepts_nested_attributes_for :address, update_only: true

  def type
    self.kind.description
  end

  def get_address
    h = {}
    h[:street] = self.address.street
    h[:city] = self.address.city
    h
  end

  # def as_json(option={})
  #   super(
  #       except: [:kind_id],
  #       include: [:kind => {:only => :description}, :phone => {:only => :number}, :address =>  {:only => [:street, :city]}]
  #       # methods: [:type, :get_address],
  #       # include: {kind: {only: :description}}
  #   )
  #
  # end

  def as_json(options = {})
    h = super(options)
    h[:birthdate] = (I18n.l(self.birthdate) unless self.birthdate.blank?)
    h
  end

end
